package ru.toppink.bustracker.config;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import ru.toppink.bustracker.model.Bus;
import ru.toppink.bustracker.model.Route;
import ru.toppink.bustracker.repository.BusRepository;
import ru.toppink.bustracker.repository.RouteRepository;
import java.util.List;
import java.util.stream.Collectors;

@Configuration
@RequiredArgsConstructor
public class DemoConfig {

    private final BusRepository busRepository;
    private final RouteRepository routeRepository;

    @EventListener(ApplicationReadyEvent.class)
    public void onReady() {
        List<Integer> routesId = routeRepository.saveAll(
                List.of(
                        new Route(null, "12", 24, 5),
                        new Route(null, "99с", 12, 3),
                        new Route(null, "1", 13.2F, 5)
                )
        ).stream().map(Route::getId).collect(Collectors.toList());

        List<Bus> buses = List.of(
                new Bus(null, routesId.get(0), "ПАЗ-3204-15", 1.2F, "Алексей", "Петрович"),
                new Bus(null, routesId.get(1), "ПАЗ-3204-18", 1.2F, "Нусратилло", "Григорян"),
                new Bus(null, routesId.get(2), "ПАЗ-3203-38", 1.1F, "Джонидеп", "Геловани"),
                new Bus(null, routesId.get(0), "ЛИАЗ-5256-871", 2.2F, "Срапион", "Жоржолиани"),
                new Bus(null, routesId.get(1), "ЛИАЗ-6213-101", 3.4F, "Чынасыл", "Чыныбаев"),
                new Bus(null, routesId.get(2), "ПАЗ-3204-27", 1.2F, "Панкратий", "Федотов"),
                new Bus(null, routesId.get(0), "ПАЗ-3204-19", 1.2F, "Лазарь", "Филатов"),
                new Bus(null, routesId.get(1), "ПАЗ-3203-21", 1.1F, "Венедикт", "Лаврентьев"),
                new Bus(null, routesId.get(2), "ЛИАЗ-5256-997", 2.2F, "Александр", "Данилов"),
                new Bus(null, routesId.get(0), "ЛИАЗ-6213-104", 3.4F, "Евдоким", "Носков"),
                new Bus(null, routesId.get(1), "ПАЗ-3204-01", 1.2F, "Исак", "Тетерин"),
                new Bus(null, routesId.get(0), "ПАЗ-3204-24", 1.2F, "Илья", "Стрелков"),
                new Bus(null, routesId.get(2), "ПАЗ-3203-77", 1.1F, "Фрол", "Доронин"),
                new Bus(null, null, "ЛИАЗ-5256-027", 2.2F, "Пантелей", "Крылов"),
                new Bus(null, null, "ЛИАЗ-6213-109", 3.4F, "Григорий", "Доронин")
        );

        busRepository.saveAll(buses);
    }
}
