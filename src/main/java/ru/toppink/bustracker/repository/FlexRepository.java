package ru.toppink.bustracker.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import java.util.List;

@NoRepositoryBean
public interface FlexRepository<MODEL, ID> extends JpaRepository<MODEL, ID> {

    List<MODEL> findAllBySubstring(
            @Param("limit") Integer limit,
            @Param("offset") Integer offset,
            @Param("str") String str
    );
}
