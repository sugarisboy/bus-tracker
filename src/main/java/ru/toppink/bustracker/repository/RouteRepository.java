package ru.toppink.bustracker.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.toppink.bustracker.model.Route;
import java.util.List;

@Repository
public interface RouteRepository extends FlexRepository<Route, Integer> {

    @Query(nativeQuery = true, value = "" +
            "SELECT * " +
            "FROM route r " +
            "WHERE :str = '' " +
            "   OR LOWER(r.number) LIKE REPLACE('%filter%', 'filter', LOWER(:str)) " +
            "ORDER BY r.number " +
            "LIMIT :limit " +
            "OFFSET :offset "
    )
    List<Route> findAllBySubstring(
            @Param("limit") Integer limit,
            @Param("offset") Integer offset,
            @Param("str") String str
    );

    @Modifying
    @Transactional
    @Query(nativeQuery = true, value = "" +
            "DELETE FROM route WHERE id = :routeId")
    void deleteById(
            @Param("routeId") Integer routeId
    );
}
