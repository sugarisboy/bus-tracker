package ru.toppink.bustracker.repository;

import io.swagger.models.auth.In;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.toppink.bustracker.model.Bus;
import java.util.List;
import java.util.Optional;

@Repository
public interface BusRepository extends FlexRepository<Bus, Integer> {

    @Query(nativeQuery = true, value = "" +
            "SELECT * " +
            "FROM bus b " +
            "WHERE :str = '' " +
            "LIMIT :limit " +
            "OFFSET :offset "
    )
    List<Bus> findAllBySubstring(
            @Param("limit") Integer limit,
            @Param("offset") Integer offset,
            @Param("str") String str
    );

    @Query(nativeQuery = true, value = "" +
            "SELECT * " +
            "FROM bus b " +
            "WHERE route_id = :routeId "
    )
    List<Bus> findAllByRoute(
            @Param("routeId") Integer routeId
    );
}
