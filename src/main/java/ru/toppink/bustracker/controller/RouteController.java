package ru.toppink.bustracker.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import ru.toppink.bustracker.dto.RouteCreateDto;
import ru.toppink.bustracker.model.Bus;
import ru.toppink.bustracker.model.Route;
import ru.toppink.bustracker.repository.BusRepository;
import ru.toppink.bustracker.repository.FlexRepository;
import ru.toppink.bustracker.repository.RouteRepository;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/route")
@RequiredArgsConstructor
public class RouteController extends FlexController<Route, RouteCreateDto> {

    private final ObjectMapper objectMapper;
    private final RouteRepository routeRepository;
    private final BusRepository busRepository;

    @Override
    Route convertCreateDtoToModel(RouteCreateDto routeCreateDto) {
        return objectMapper.convertValue(routeCreateDto, Route.class);
    }

    @Override
    @DeleteMapping("/{id}")
    public void delete(
            @PathVariable Integer id
    ) {
        List<Bus> busByRoute = busRepository.findAllByRoute(id);

        if (!busByRoute.isEmpty()) {
            String message = "Нельзя удалить маршрут, так как к нему привязаны автобусы: " +
                    busByRoute.stream()
                            .map(Bus::getId)
                            .map(Objects::toString)
                            .collect(Collectors.joining(", "));
            throw new HttpClientErrorException(HttpStatus.CONFLICT, message);
        }

        super.delete(id);
    }

    @Override
    FlexRepository<Route, Integer> getRepository() {
        return routeRepository;
    }
}
