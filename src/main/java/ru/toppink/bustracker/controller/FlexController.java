package ru.toppink.bustracker.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import ru.toppink.bustracker.repository.FlexRepository;
import java.util.List;
import javax.validation.Valid;

public abstract class FlexController<MODEL, CREATE_DTO> {

    @GetMapping
    @ApiOperation("Поиск всех записей")
    public List<MODEL> findAll(
            @ApiParam("Максимум выводимых записей")
            @RequestParam(defaultValue = "10") int limit,

            @ApiParam("Начинать поиск с N-ой записи")
            @RequestParam(defaultValue = "0") int offset,

            @ApiParam("Искать начинающиеся с данной подстроки")
            @RequestParam(defaultValue = "") String str
    ) {
        return getRepository().findAllBySubstring(limit, offset, str);
    }

    @GetMapping("/{id}")
    @ApiOperation("Получение конкретной записи по ID")
    public MODEL findById(
            @ApiParam("ID сущности")
            @PathVariable Integer id
    ) {
        return getRepository().findById(id).orElseThrow(
                () -> new HttpClientErrorException(HttpStatus.NOT_FOUND, "Entity not found"));
    }

    @PutMapping
    @ApiOperation("Создание новой сущности")
    public MODEL create(
            @Valid @RequestBody CREATE_DTO createDto
    ) {
        MODEL model = convertCreateDtoToModel(createDto);
        return getRepository().save(model);
    }

    /*@PatchMapping("/{id}")
    public MODEL update(
            @Valid @RequestBody UPDATE_DTO updateDto,
            @PathVariable Integer id
    ) {
        MODEL model = getRepository().findById(id).orElseThrow(
                () -> new HttpClientErrorException(HttpStatus.NOT_FOUND, "Сущность не найдена"));
        MODEL updatedModel = updateFromDto(model, updateDto);
        return getRepository().save(updatedModel);
    }*/

    @DeleteMapping("/{id}")
    @ApiOperation("Удаление конкретной записи по ID")
    public void delete(
            @ApiParam("ID сущности")
            @PathVariable Integer id
    ) {
        getRepository().deleteById(id);
    }

    abstract MODEL convertCreateDtoToModel(CREATE_DTO createDto);

    /*abstract MODEL updateFromDto(MODEL model, UPDATE_DTO updateDto);*/

    abstract FlexRepository<MODEL, Integer> getRepository();
}
