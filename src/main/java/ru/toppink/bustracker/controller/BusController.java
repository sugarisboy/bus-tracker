package ru.toppink.bustracker.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.toppink.bustracker.dto.BusCreateDto;
import ru.toppink.bustracker.model.Bus;
import ru.toppink.bustracker.repository.BusRepository;
import ru.toppink.bustracker.repository.FlexRepository;
import ru.toppink.bustracker.repository.RouteRepository;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/bus")
@RequiredArgsConstructor
public class BusController extends FlexController<Bus, BusCreateDto> {

    private final ObjectMapper objectMapper;
    private final BusRepository busRepository;
    private final RouteRepository routeRepository;

    @Override
    Bus convertCreateDtoToModel(BusCreateDto busCreateDto) {
        return objectMapper.convertValue(busCreateDto, Bus.class);
    }

    @Override
    @DeleteMapping("/{id}")
    public void delete(
            @PathVariable Integer id
    ) {
        super.delete(id);
    }

    @GetMapping("/filter")
    @ApiModelProperty("Поиск автобусов по фильтру")
    public List<Bus> filterBuses(
            @ApiParam("Фильтрация по фамилии водителя")
            @RequestParam String driverLastNameStartWith
    ) {
        return busRepository.findAll()
                .stream()
                .filter(bus -> bus.getDriverLastName().toLowerCase().startsWith(driverLastNameStartWith.toLowerCase()))
                .collect(Collectors.toList());
    }

    @Override
    FlexRepository<Bus, Integer> getRepository() {
        return busRepository;
    }
}
