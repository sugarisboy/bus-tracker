/*
package ru.toppink.bustracker.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.AbstractPropertyBindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpStatusCodeException;
import java.util.HashMap;
import java.util.Map;
import javax.validation.ValidationException;

@Slf4j
@ControllerAdvice
public class ExceptionController {

    @ExceptionHandler(HttpStatusCodeException.class)
    public ResponseEntity<Object> handleHttpClientException(HttpStatusCodeException ex) {
        String message = ex.getMessage();
        HttpStatus statusCode = ex.getStatusCode();

        Map<String, Object> body = Map.of(
                "message", message,
                "status", statusCode,
                "stack", ex.getStackTrace()
        );

        log.error("Exception: {}", body);
        return new ResponseEntity<>(body, statusCode);
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity<Object> handleHttpClientException(ValidationException ex) {
        String message = ex.getMessage();

        Map<String, Object> body = Map.of(
                "message", message,
                "status", HttpStatus.BAD_REQUEST,
                "stack", ex.getStackTrace()
        );

        log.error("Exception: {}", body);
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException .class)
    public ResponseEntity<Object> handleHttpClientException(MethodArgumentNotValidException ex) {

        Map<String, Object> body = new HashMap<>();

        for (Object obj : ex.getBindingResult().getModel().values()) {
            if (obj instanceof AbstractPropertyBindingResult) {
                AbstractPropertyBindingResult result = (AbstractPropertyBindingResult) obj;
                ObjectError objError = result.getAllErrors().get(0);
                if (objError instanceof FieldError) {
                    FieldError error = (FieldError) objError;
                    String message = error.getDefaultMessage();
                    String field = error.getField();
                    body = Map.of(
                            "message", message,
                            "status", HttpStatus.BAD_REQUEST,
                            "stack", ex.getStackTrace()
                    );
                } else {
                    ObjectError error = objError;
                    String message = error.getDefaultMessage();
                    body = Map.of(
                            "message", message,
                            "status", HttpStatus.BAD_REQUEST,
                            "stack", ex.getStackTrace()
                    );
                }
            }
        }

        log.error("Exception: {}", ex);
        return new ResponseEntity<>(body, HttpStatus.BAD_REQUEST);
    }
}
*/
