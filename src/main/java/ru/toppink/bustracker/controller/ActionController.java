package ru.toppink.bustracker.controller;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import ru.toppink.bustracker.dto.FuelConsumptionDto;
import ru.toppink.bustracker.dto.FuelRouteConsumptionDto;
import ru.toppink.bustracker.dto.RouteInspectDto;
import ru.toppink.bustracker.model.Bus;
import ru.toppink.bustracker.model.Route;
import ru.toppink.bustracker.repository.BusRepository;
import ru.toppink.bustracker.repository.RouteRepository;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/action")
@RequiredArgsConstructor
public class ActionController {

    private final BusRepository busRepository;
    private final RouteRepository routeRepository;

    @PostMapping("/set-route")
    @ApiOperation("Привязать автобус к маршруту")
    public void changeRouteForBus(
            @ApiParam("ID автобуса")
            @RequestParam Integer busId,
            @ApiParam("ID маршрута")
            @RequestParam Integer routeId
    ) {
        routeRepository.findById(routeId)
                .orElseThrow(() -> new HttpClientErrorException(HttpStatus.NOT_FOUND, "Маршрут не найден"));

        Bus bus = busRepository.findById(busId)
                .orElseThrow(() -> new HttpClientErrorException(HttpStatus.NOT_FOUND, "Автобус не найден"));

        bus.setRouteId(routeId);
        busRepository.save(bus);
    }

    @PostMapping("/drop-route")
    @ApiOperation("Отвязать маршрут от автобуса")
    public void dropRouteForBus(
            @ApiParam("ID автобуса")
            @RequestParam Integer busId
    ) {
        Bus bus = busRepository.findById(busId)
                .orElseThrow(() -> new HttpClientErrorException(HttpStatus.NOT_FOUND, "Автобус не найден"));

        bus.setRouteId(null);
        busRepository.save(bus);
    }

    @GetMapping("/route-inspect/{routeId}")
    @ApiOperation("Инспектировать маршрут")
    public RouteInspectDto inspectRoute(
            @ApiParam("ID маршрута")
            @PathVariable Integer routeId
    ) {
        Route route = routeRepository.findById(routeId)
                .orElseThrow(() -> new HttpClientErrorException(HttpStatus.NOT_FOUND, "Маршрут не найден"));

        List<Bus> buses = busRepository.findAllByRoute(routeId);

        return new RouteInspectDto(
                buses,
                buses.size(),
                route.getNeedBusCount()
        );
    }

    @GetMapping("/fuel-consumption")
    @ApiOperation("Узнать потребление топлива")
    public FuelConsumptionDto findFuelConsumption() {
        Map<Integer, List<Bus>> busByRoute = busRepository.findAll()
                .stream()
                .filter(bus -> Objects.nonNull(bus.getRouteId()))
                .collect(Collectors.groupingBy(Bus::getRouteId));

        Map<Integer, Route> routes = busByRoute.keySet()
                .stream()
                .map(routeId -> routeRepository.findById(routeId).orElseThrow())
                .collect(Collectors.toMap(Route::getId, i -> i));

        List<FuelRouteConsumptionDto> consumptions = busByRoute.keySet().stream().map(routeId -> {
            List<Bus> busesForThisRoute = busByRoute.get(routeId);
            Route route = routes.get(routeId);

            float sumFuelPerKm = busesForThisRoute.stream()
                    .map(Bus::getFuelPerKm)
                    .reduce(Float::sum)
                    .orElse(0F);

            float consumption = sumFuelPerKm * route.getDistance();

            return new FuelRouteConsumptionDto(route, consumption);
        }).collect(Collectors.toList());

        Float total = consumptions.stream()
                .map(FuelRouteConsumptionDto::getConsumption)
                .reduce(Float::sum)
                .orElse(0F);

        return new FuelConsumptionDto(total, consumptions);
    }
}
