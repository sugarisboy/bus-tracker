package ru.toppink.bustracker.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.toppink.bustracker.model.Route;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FuelRouteConsumptionDto {

    @ApiModelProperty("Маршрут")
    private Route route;

    @ApiModelProperty("Суммарное потребление топлива (потребление каждого автобуса * протяженность маршрута)")
    private float consumption;
}
