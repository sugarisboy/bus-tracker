package ru.toppink.bustracker.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import ru.toppink.bustracker.model.Bus;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Ответ на инспекцию маршрута")
public class RouteInspectDto {

    @ApiModelProperty("Список автобусов на маршруте")
    private List<Bus> buses;

    @ApiModelProperty("Количество автобусов на маршруте")
    private int busCount;

    @ApiModelProperty("Количество необходимых автобусов")
    private int needBusCount;
}
