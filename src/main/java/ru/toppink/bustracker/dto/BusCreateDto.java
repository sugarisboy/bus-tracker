package ru.toppink.bustracker.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Создание автобуса")
public class BusCreateDto {

    @ApiModelProperty("Номер автобуса")
    @NotBlank(message = "Номер автобуса должен быть указан")
    private String number;

    @ApiModelProperty("Расход топлива на км")
    @NotNull(message = "Расход топлива должен быть указан")
    @Positive(message = "Расход топлива должен быть положительным")
    private float fuelPerKm;

    @ApiModelProperty("Имя водителя")
    @NotBlank(message = "Имя водителя должно быть указан")
    private String driverFirstName;

    @ApiModelProperty("Фамилия водителя")
    @NotBlank(message = "Фамилия водителя должна быть указана")
    private String driverLastName;
}
