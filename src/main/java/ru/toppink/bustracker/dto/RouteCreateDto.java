package ru.toppink.bustracker.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ApiModel("Создание маршрута")
public class RouteCreateDto {

    @ApiModelProperty("Номер маршрута (1-7 символ)")
    @NotBlank(message = "Название не может быть пустым")
    @Size(min = 1, max = 7, message = "Размер названия слишком большой или слишком маленький")
    private String number;

    @ApiModelProperty("Протяженность маршрута")
    @Positive(message = "Дистанция должна быть положительной")
    private float distance;

    @ApiModelProperty("Количество необходимых автобусов")
    @Positive(message = "Количество необходимых автобусов должно быть больше нуля")
    private int needBusCount;
}
