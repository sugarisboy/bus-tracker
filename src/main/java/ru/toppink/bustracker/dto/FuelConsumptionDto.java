package ru.toppink.bustracker.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class FuelConsumptionDto {

    @ApiModelProperty("Общий расход бензина по всем маршрутам")
    private float total;

    @ApiModelProperty("Потребление топлива на конкретном маршруте")
    private List<FuelRouteConsumptionDto> routes;
}
