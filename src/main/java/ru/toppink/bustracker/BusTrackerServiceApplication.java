package ru.toppink.bustracker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BusTrackerServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(BusTrackerServiceApplication.class, args);
	}

}
