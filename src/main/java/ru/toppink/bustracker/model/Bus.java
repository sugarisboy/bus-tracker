package ru.toppink.bustracker.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Builder
@Table(name = "bus")
@NoArgsConstructor
@AllArgsConstructor
public class Bus {

    @Id
    @GeneratedValue
    private Integer id;

    private Integer routeId;

    private String number;

    private float fuelPerKm;

    private String driverFirstName;

    private String driverLastName;
}

