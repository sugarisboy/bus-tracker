FROM adoptopenjdk/openjdk11:alpine-jre

ENV SERVICE_NAME=bus-tracker

ENV SPRING_JPA_PROPERTIES_HIBERNATE_DEFAULT_SCHEMA=bus_tracker
ENV SPRING_FLYWAY_SCHEMAS=bus_tracker

ADD target/bus-tracker-0.0.1-SNAPSHOT.jar /app.jar

ENTRYPOINT exec java -jar /app.jar
